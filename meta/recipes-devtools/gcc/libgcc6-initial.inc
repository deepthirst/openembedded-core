require libgcc6-common.inc

DEPENDS = "virtual/${TARGET_PREFIX}gcc"

LICENSE = "GPL-3.0-with-GCC-exception"

PACKAGES = ""

HOST_ARCH = "${BUILD_ARCH}"
HOST_VENDOR = "${BUILD_VENDOR}"
HOST_OS = "${BUILD_OS}"
HOST_PREFIX = "${BUILD_PREFIX}"
HOST_CC_ARCH = "${BUILD_CC_ARCH}"
HOST_LD_ARCH = "${BUILD_LD_ARCH}"
HOST_AS_ARCH = "${BUILD_AS_ARCH}"

EXTRA_OECONF += "--disable-shared"

inherit nopackages

# We really only want this built by things that need it, not any recrdeptask
deltask do_build

do_configure_prepend () {
	install -d ${STAGING_INCDIR}
	touch ${STAGING_INCDIR}/limits.h
	sed -i -e 's#INHIBIT_LIBC_CFLAGS =.*#INHIBIT_LIBC_CFLAGS = -Dinhibit_libc#' ${B}/gcc/libgcc.mvars
	sed -i -e 's#inhibit_libc = false#inhibit_libc = true#' ${B}/gcc/Makefile
}

do_configure_append () {
	sed -i -e 's#thread_header = .*#thread_header = gthr-single.h#' ${B}/${BPN}/Makefile
}

do_install_append () {
	ln -s libgcc.a ${D}${libdir}/${TARGET_SYS}/${BINV}/libgcc_eh.a
}
